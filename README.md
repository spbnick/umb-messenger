# umb-messenger

Webhook responsible for UMB message sending to trigger [external testing].

## How to run this project

The messenger can be deployed locally by running

```bash
python -m umb_messenger
```

Note that you need the appropriate environment variables exposed and have UMB
certificate and configuration in the right place!

Required environment variables:

* `CKI_LOGGING_LEVEL`: logging level for CKI modules, defaults to WARN; to get
  meaningful output on the command line, set to INFO
* `DATAWAREHOUSE_URL`: URL of the DataWarehouse instance to use, defaults to `localhost`
* `DATAWAREHOUSE_TOKEN_UMB_MESSENGER`: Access token to the DataWarehouse instance

To receive messages via RabbitMQ, the following environment variables need to
be set as well:

* `RABBITMQ_HOST`: space-separated list of AMQP servers
* `RABBITMQ_PORT`: AMQP port
* `RABBITMQ_USER`: user name of the AMQP account
* `RABBITMQ_PASSWORD`: password of the AMQP account
* `UMB_MESSENGER_EXCHANGE`: name of the webhook exchange, defaults to
  `cki.exchange.datawarehouse.kcidb`
* `UMB_MESSENGER_QUEUE`: queue name, defaults to `cki.queue.datawarehouse.kcidb.umb_messenger`

### Manual triggering

Messaging can also be manually triggered via the command line. First, make
sure to have the required environment variables above configured correctly.
Then, the sending of UMB messages can be triggered with

```bash
CKI_LOGGING_LEVEL=INFO \
  CKI_DEPLOYMENT_ENVIRONMENT=production \
  python3 -m umb_messenger \
  --message-type MESSAGE_TYPE \
  --checkout-id CHECKOUT_ID
```

* `MESSAGE_TYPE` can be `pre_test` or `post_test`.
* `CHECKOUT_ID` is the KCIDB checkout ID.

[external testing]: https://cki-project.org/docs/test-maintainers/testing-builds
