"""Message templates."""
# Message template for ready_for_test. "None" fields mark parts that are filled
# out based on actual pipeline data.
READY_FOR_TEST = {
    'ci': {
        'name': 'CKI (Continuous Kernel Integration)',
        'team': 'CKI',
        'docs': 'https://cki-project.org',
        'url': 'https://gitlab.com/cki-project',
        'irc': 'Slack #team-kernel-cki',
        'email': 'cki-project@redhat.com'
    },
    'run': {
        'url': None,
    },
    'artifact': {
        'type': 'cki-build',
        'issuer': None,
        'component': None,
    },
    'system': [{
        'os': None,
        'stream': None
    }],
    'checkout_id': None,
    'build_info': None,
    'patch_urls': None,
    'merge_request': {
        'merge_request_url': None,
        'is_draft': None,
        'subsystems': None,
        'jira': None,
        'bugzilla': None
    },
    'branch': None,
    'modified_files': None,
    'cki_finished': None,
    'type': 'build',
    'category': 'kernel-build',
    'status': None,
    'namespace': 'cki',
    'generated_at': None,
    'version': '0.1.0'

}
PRE_TEST = READY_FOR_TEST
POST_TEST = READY_FOR_TEST
