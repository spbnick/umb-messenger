"""Functionality related to UMB message sending."""
from copy import deepcopy

from cki_lib import misc
from cki_lib import stomp

from . import message_data
from . import templates
from .settings import LOGGER


def handle_message(checkout, message_type):
    """
    Initialize UMB client, build and send the message and end the connection.

    Args:
        checkout:     KCIDB checkout data, as return by DataWarehouse API.
        message_type: Type of the message to send.
    """
    LOGGER.info('Gathering data for %s: %s', message_type, checkout.id)

    # Get the template, but don't overwrite it!
    message = deepcopy(getattr(templates, message_type.upper()))

    full_message = message_data.get_ready_for_test_data(message, message_type, checkout)

    if 'error' in full_message or 'reason' in full_message:  # Something went wrong
        LOGGER.info('Not sending messages for errors')
        return

    topic = '/topic/VirtualTopic.eng.cki.ready_for_test'

    if misc.is_production():
        LOGGER.info('Sending message to %s', topic)
        stomp.StompClient().send_message(full_message, topic)
    else:
        LOGGER.info('Production mode would send %s to %s', full_message, topic)
